#!/bin/sh
set -eu

# Download exit list
curl -s --ssl-reqd https://check.torproject.org/exit-addresses -o /tmp/tor-exits-raw

# Format the list for Nginx
TOREXITLIST=$(grep "ExitAddress" /tmp/tor-exits-raw | awk '{print "\t" $2 " 1;"}' | sort | uniq)

# Pipe the output into an Nginx-compatible config file
echo -e "geo \$torExit {
\tdefault 0;
$TOREXITLIST
}" > /tmp/torexits.conf